var app = angular.module('myApp', [
  'ngRoute'
]);

app.config(function($routeProvider, $httpProvider) {
  $httpProvider.defaults.withCredentials = true;
  $httpProvider.xsrfWhitelistedOrigins = "http://localhost:1999"
  $routeProvider

  .when('/', {
    templateUrl : 'html/home.html',
  })

  .when('/userhome', {
    templateUrl : 'html/userhome.html',
  })

  .when('/profile',{
    templateUrl : 'html/profile.html',
    controller : 'UserHomeController'
  })

  .when('/stats',{
    templateUrl : 'html/stats.html',
  })

  .when('/editPosts',{
    templateUrl : 'html/editPosts.html',
  })


  .otherwise({redirectTo: '/'});
});

var userName = ""
app.controller("HomeController", function ($scope, $http, $location, $rootScope, $routeParams) {
  $scope.signup = function () {
      if($scope.signup.userName=="" ||
          $scope.signup.password=="" ||
          $scope.signup.bio=="")
          $.notify("Fill all Details ","error")
      else{
        var headers = {
          'Access-Control-Allow-Origin' : '*',
          'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        };
      var req = {
          method: 'POST',

          url: 'http://localhost:1999/register',
          header:headers,
          data: {
  
              "authDetails" : {
              "userName" : $scope.signup.userName,
              "password" : $scope.signup.password
              },
              "userDetails" : {
                "userName" : $scope.signup.userName,
                "bio"	: $scope.signup.bio
              }
          }
      }
      //console.log(req);
      $http(req).then(function successCallback(response) {
          $scope.response = response.data;
          console.log($scope.response)
          if (response.data == true) {
              //$.notify("Try Login","success");
              $.notify("Registeration Successful Login to continue","success");
              console.log("Registration success")
              $location.path('/home')
          }
          if(response.data=="false")
              $.notify("Registeration Unsuccessful","info");
          // $scope.signup.message = "SUCCESSFULLY SIGNED UP!!!GO BACK TO HOME PAGE AND CHECK LOGIN"

      }, function errorCallback(response) {
          $scope.errorStatus = response.status;
          console.log(response.status);
          if (response.status == 401) {
              $location.path('/home')
          }
          if (response.status == 400) {
              $.notify("Bad Request ", "error");
          }
      })
      }
  }

  $scope.login = function () {
    var req = {
        method: 'POST',
        url: 'http://localhost:1999/login',
        data: {
          "authDetails" : {
            "userName" : $scope.login.userNameLogin,
            "password" : $scope.login.passwordLogin
            }
        }
    }
    //console.log(req);
    $http(req).then(function successCallback(response) {
        $scope.response = response.data;
        console.log($scope.response)
        if ($scope.response == "false") {
            $scope.login.message = "INVALID DETAILS!!!"
            //$.notify("Invalid Details","error");
            $.notify("Invalid Details ", "error");
        }
        else
        { 
          userName = $scope.login.userNameLogin
          $location.path('/userhome')
          //$location.path("/user/" + $scope.response + "/options") 
        }
    }, function errorCallback(response) {
        $scope.errorStatus = response.status;
        console.log(response.status);
        if (response.status == 401) {
            $.notify("Invalid login credentials", "error");
            $location.path('/home')
        }
        if (response.status == 400) {
            //$.notify("Bad Request ","error");
            $.notify("Bad Request ", "error");
        }
    })

}

  $scope.back = function () {
      $location.path("/home")

  }
});
app.controller("UserHomeController", function ($scope, $http, $location, $rootScope, $routeParams) {

  var headers = {
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
  var req = {
    method: 'GET',
    url: 'http://localhost:1999/posts'
  }
  $http(req).then(function successCallback(response) {
    $scope.response = response.data;
    if (response.data) {
      $scope.postData = response.data ;
      // console.log($scope.postData)
    }
  },function errorCallback(response) {
      $scope.errorStatus = response.status;
      console.log(response.status);
      if (response.status == 401) {
          $location.path('/home')
      }
      if (response.status == 400) {
          $.notify("Bad Request ", "error");
      }
  })
  $scope.loadComments = function(){
      req = {
        method: 'GET',
        url: 'http://localhost:1999/comments'
      }
      $http(req).then(function successCallback(response) {
        $scope.response = response.data;
        if (response.data) {
          $scope.commentData = response.data ;
          // console.log($scope.postData)
        }
      },function errorCallback(response) {
          $scope.errorStatus = response.status;
          console.log(response.status);
          if (response.status == 401) {
              $location.path('/home')
          }
          if (response.status == 400) {
              $.notify("Bad Request ", "error");
          }
      })
  }

  $scope.loadComments() ;



  $scope.addComment = function (post) {
      var requestUrl = 'http://localhost:1999/posts/'+post.data.postId+'/addComment'
      var req = {
          method: 'POST',
          url: requestUrl,
          data: {
            "commentDetails" : {
              "userName" : userName,
              "content" : post.commentContent,
              "postId" : post.data.postId
            }
          }
      }
      //console.log(req);
      $http(req).then(function successCallback(response) {
          $scope.response = response.data;
          console.log($scope.response)
          if ($scope.response == "false") {
              $scope.login.message = "INVALID DETAILS!!!"
              //$.notify("Invalid Details","error");
              $.notify("Invalid Details ", "error");
          }
          else
          { 
            $scope.loadComments()
          }
      }, function errorCallback(response) {
          $scope.errorStatus = response.status;
          console.log(response.status);
          if (response.status == 401) {
              $.notify("Invalid comment Details", "error");
              $location.path('/home')
          }
          if (response.status == 400) {
              //$.notify("Bad Request ","error");
              $.notify("Bad Request","error");
          }
      })

  }

  //$scope.message =  res;
});

app.controller("ProfileController", function ($scope, $http, $location, $rootScope, $routeParams) {

});

app.controller("EditPostsController", function ($scope, $http, $location, $rootScope, $routeParams) {
  
});

app.controller("StatsController", function ($scope, $http, $location, $rootScope, $routeParams) {
  $scope.vijay = 100;
  $scope.pavithran = function(){

  }
  $rootScope.ganesh = 100 ;
});


